#Author: Alessandra Santos Costa (e-mail:alebsiufu@gmail.com)

@validateMenu
Feature: Validate Menu
	As a user
	I want to access the menu page
	So I can choose which page I want to access

@infoStationMenu	
Scenario: Access Information Station Menu
	Given the user accesses the Autonomiae Auream home page menu
	When the user clicks the Information Station Menu
	Then the user is directed to the information station "information station" page
	
@contactMenu	
Scenario: Access Information Contact Menu
	Given the user accesses the Autonomiae Auream home page menu
	When the user clicks the Contact Menu
	Then the user is directed to the contact "contact" page

@homeMenu	
Scenario: Access Home Menu
	Given the user accesses the Autonomiae Auream home page menu
	When the user clicks the Contact Menu
	And the user clicks the Home Menu 
	Then the user is directed to the home "home" page
	