#Author: Alessandra Santos Costa (e-mail:alebsiufu@gmail.com)

@validateContact
Feature: Validate Contact
	As a user
	I want to access the contact page
	So I can send my contact

@contactValidate	
Scenario: Validate Contact
	Given the user accesses the Contact page
	When the user enters the contact information
	Then the user is directed to the "information station" page and the contact is successfully sent

@invalidEmailContact	
Scenario Outline: Invalid Email
	Given the user accesses the Contact page
	When the user enters an invalid "<email>"
	Then the user is not directed to the "information station" page and the contact is not successfully sent
	
	Examples: 
			|    email    |
			|             |
      |    alebsi   |
      |   alebsi@   |

@emptyFirstName      
Scenario: Empty First Name Field
	Given the user accesses the Contact page
	When the user enters an empty first name "First Name" Field
	Then the user is not directed to the "information station" page and the contact is not successfully sent

@emptyLastName	
Scenario: Empty Last Name Field
	Given the user accesses the Contact page
	When the user enters an empty last name "Last Name" Field
	Then the user is not directed to the "information station" page and the contact is not successfully sent

@emptyPhoneNumber	
Scenario: Empty Phone Number Field
	Given the user accesses the Contact page
	When the user enters an empty phone number "Phone Number" Field
	Then the user is not directed to the "information station" page and the contact is not successfully sent		

@emptyComment	
Scenario: Empty Comment
	Given the user accesses the Contact page
	When the user enters an empty comment "comment" Field
	Then the user is directed to the "information station" page and the comment is empty in url