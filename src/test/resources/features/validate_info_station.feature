#Author: Alessandra Santos Costa (e-mail:alebsiufu@gmail.com)

@validateInfoStation
Feature: Validate Information Station Page
	As a user
	I want to access the Information Station Page
	So I can view the information on the page

@infoStationValidate	
Scenario: Access Information Station Page
	Given the user accesses the Autonomiae Auream system
	When the user clicks the Information Station menu option
	Then the user is directed to the information station "information station" page and view the page information successfully