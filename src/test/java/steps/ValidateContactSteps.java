package steps;

import pageObjects.ValidateContact;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

import junit.framework.Assert;

public class ValidateContactSteps {
	
	private WebDriver driver;
	
	@Given("^the user accesses the Contact page$")
	public void aUser() throws Throwable {
		JSONParser parser = new JSONParser();
		
		Object object = parser
            .parse(new FileReader("C:\\Users\\Alessandra\\eclipse-workspace\\TestMetixMedical\\asserts\\env.json"));
    
		JSONObject jsonObject = (JSONObject)object;
    
		String chromeDriver = (String) jsonObject.get("chromeDriver");
		String contactPage = (String) jsonObject.get("contactPage");
			System.setProperty("webdriver.chrome.driver", chromeDriver);
			driver = new ChromeDriver();
			driver.get(contactPage);
			driver.manage().window().maximize();
			Thread.sleep(3000);
	
	}
	
	@When("^the user enters the contact information$")
	public void theUserEntersTheContactInformation() throws Throwable {
		ValidateContact.emailAddressInput(driver).click();
		ValidateContact.emailAddressInput(driver).sendKeys("alebsiufu@gmail.com");
		ValidateContact.firstNameInputField(driver).click();
		ValidateContact.firstNameInputField(driver).sendKeys("Alessandra");
		ValidateContact.lastNameInputField(driver).click();
		ValidateContact.lastNameInputField(driver).sendKeys("Costa");
		ValidateContact.phoneNumberInputField(driver).click();
		ValidateContact.phoneNumberInputField(driver).sendKeys("5534988759228");
		ValidateContact.requestTypeTextBox(driver).click();
		ValidateContact.requestTypeTextBoxOption(driver).click();
		ValidateContact.commentField(driver).sendKeys("Metix Medical Test");
		ValidateContact.submitContactButton(driver).click();
		
		Thread.sleep(3000);
	
	}

	@Then("^the user is directed to the \"([^\"]*)\" page and the contact is successfully sent$")
	public void theUserIsDirectedToThePageAndTheContactIsSuccessfullySent(String arg1) throws Throwable {
		JSONParser parser = new JSONParser();
		
		Object object = parser
            .parse(new FileReader("C:\\Users\\Alessandra\\eclipse-workspace\\TestMetixMedical\\asserts\\env.json"));
    
		JSONObject jsonObject = (JSONObject)object;
    
		String contactMessage = (String) jsonObject.get("contactMessage");
		
		String currentUrl = driver.getCurrentUrl();
		Assert.assertEquals(contactMessage, currentUrl);
		
	}
	
	@When("^the user enters an invalid \"([^\"]*)\"$")
	public void theUserEntersAnInvalid(String arg1) throws Throwable {
		ValidateContact.emailAddressInput(driver).click();
		ValidateContact.emailAddressInput(driver).sendKeys(arg1);
		ValidateContact.firstNameInputField(driver).sendKeys("Alessandra");
		ValidateContact.lastNameInputField(driver).click();
		ValidateContact.lastNameInputField(driver).sendKeys("Costa");
		ValidateContact.phoneNumberInputField(driver).click();
		ValidateContact.phoneNumberInputField(driver).sendKeys("5534988759228");
		ValidateContact.requestTypeTextBox(driver).click();
		ValidateContact.requestTypeTextBoxOption(driver).click();
		ValidateContact.commentField(driver).sendKeys("Metix Medical Test");
		ValidateContact.submitContactButton(driver).click();
		
	}

	@Then("^the user is not directed to the \"([^\"]*)\" page and the contact is not successfully sent$")
	public void theUserIsNotDirectedToThePageAndTheContactIsNotSuccessfullySent(String arg1) throws Throwable {
		JSONParser parser = new JSONParser();
		
		Object object = parser
            .parse(new FileReader("C:\\Users\\Alessandra\\eclipse-workspace\\TestMetixMedical\\asserts\\env.json"));
    
		JSONObject jsonObject = (JSONObject)object;
    
		String contactPage = (String) jsonObject.get("contactPage");
		
		String currentUrl = driver.getCurrentUrl();
		Assert.assertEquals(contactPage, currentUrl);
		Thread.sleep(3000);
		
	}
	
	@When("^the user enters an empty first name \"([^\"]*)\" Field$")
	public void theUserEntersAnEmptyFirstNameField(String arg1) throws Throwable {
		ValidateContact.emailAddressInput(driver).click();
		ValidateContact.emailAddressInput(driver).sendKeys("alebsiufu@gmail.com");
		ValidateContact.firstNameInputField(driver).sendKeys("");
		ValidateContact.lastNameInputField(driver).click();
		ValidateContact.lastNameInputField(driver).sendKeys("Costa");
		ValidateContact.phoneNumberInputField(driver).click();
		ValidateContact.phoneNumberInputField(driver).sendKeys("5534988759228");
		ValidateContact.requestTypeTextBox(driver).click();
		ValidateContact.requestTypeTextBoxOption(driver).click();
		ValidateContact.commentField(driver).sendKeys("Metix Medical Test");
		ValidateContact.submitContactButton(driver).click();
		
	}
	
	@When("^the user enters an empty last name \"([^\"]*)\" Field$")
	public void theUserEntersAnEmptyLastNameField(String arg1) throws Throwable {
		ValidateContact.emailAddressInput(driver).click();
		ValidateContact.emailAddressInput(driver).sendKeys("alebsiufu@gmail.com");
		ValidateContact.firstNameInputField(driver).sendKeys("Alessandra");
		ValidateContact.lastNameInputField(driver).click();
		ValidateContact.lastNameInputField(driver).sendKeys("");
		ValidateContact.phoneNumberInputField(driver).click();
		ValidateContact.phoneNumberInputField(driver).sendKeys("5534988759228");
		ValidateContact.requestTypeTextBox(driver).click();
		ValidateContact.requestTypeTextBoxOption(driver).click();
		ValidateContact.commentField(driver).sendKeys("Metix Medical Test");
		ValidateContact.submitContactButton(driver).click();
		
	}
	
	@When("^the user enters an empty phone number \"([^\"]*)\" Field$")
	public void theUserEntersAnEmptyPhoneNumberField(String arg1) throws Throwable {
		ValidateContact.emailAddressInput(driver).click();
		ValidateContact.emailAddressInput(driver).sendKeys("alebsiufu@gmail.com");
		ValidateContact.firstNameInputField(driver).sendKeys("Alessandra");
		ValidateContact.lastNameInputField(driver).click();
		ValidateContact.lastNameInputField(driver).sendKeys("Costa");
		ValidateContact.phoneNumberInputField(driver).click();
		ValidateContact.phoneNumberInputField(driver).sendKeys("");
		ValidateContact.requestTypeTextBox(driver).click();
		ValidateContact.requestTypeTextBoxOption(driver).click();
		ValidateContact.commentField(driver).sendKeys("Metix Medical Test");
		ValidateContact.submitContactButton(driver).click();
		
	}
	
	@When("^the user enters an empty comment \"([^\"]*)\" Field$")
	public void theUserEntersAnEmptyCommentField(String arg1) throws Throwable {
		ValidateContact.emailAddressInput(driver).click();
		ValidateContact.emailAddressInput(driver).sendKeys("alebsiufu@gmail.com");
		ValidateContact.firstNameInputField(driver).sendKeys("Alessandra");
		ValidateContact.lastNameInputField(driver).click();
		ValidateContact.lastNameInputField(driver).sendKeys("Costa");
		ValidateContact.phoneNumberInputField(driver).click();
		ValidateContact.phoneNumberInputField(driver).sendKeys("5534988759228");
		ValidateContact.requestTypeTextBox(driver).click();
		ValidateContact.requestTypeTextBoxOption(driver).click();
		ValidateContact.commentField(driver).sendKeys("");
		ValidateContact.submitContactButton(driver).click();
		
	}
	
	@Then("^the user is directed to the \"([^\"]*)\" page and the comment is empty in url$")
	public void theUserIsDirectedToThePageAndTheCommentIsEmptyInUrl(String arg1) throws Throwable {
		JSONParser parser = new JSONParser();
		
		Object object = parser
            .parse(new FileReader("C:\\Users\\Alessandra\\eclipse-workspace\\TestMetixMedical\\asserts\\env.json"));
    
		JSONObject jsonObject = (JSONObject)object;
    
		String contactEmptyMessage = (String) jsonObject.get("contactEmptyMessage");
		
		String currentUrl = driver.getCurrentUrl();
		Assert.assertEquals(contactEmptyMessage, currentUrl);
		Thread.sleep(3000);
		
	}
	
	@After("@contactValidate")
	public void screenshotCloseBrowsercontactValidate(Scenario scenario){
		File file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	    try {
			FileUtils.copyFile(file, new File("target/screenshots/"+scenario.getId()+".jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	    driver.close();
	    System.out.println("End"); 
	    
	}
	
	@After("@invalidEmailContact")
	public void screenshotCloseBrowserinvalidEmailContact(Scenario scenario){
		File file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	    try {
			FileUtils.copyFile(file, new File("target/screenshots/"+scenario.getId()+".jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	    driver.close();
	    System.out.println("End"); 
	    
	}
	
	@After("@emptyFirstName")
	public void screenshotCloseBrowserEmptyFirstName(Scenario scenario){
		File file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	    try {
			FileUtils.copyFile(file, new File("target/screenshots/"+scenario.getId()+".jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	    driver.close();
	    System.out.println("End"); 
	    
	}
	
	@After("@emptyLastName")
	public void screenshotCloseBrowserEmptyLastName(Scenario scenario){
		File file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	    try {
			FileUtils.copyFile(file, new File("target/screenshots/"+scenario.getId()+".jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	    driver.close();
	    System.out.println("End"); 
	    
	}
	
	@After("@emptyPhoneNumber")
	public void screenshotCloseBrowseremptyPhoneNumber(Scenario scenario){
		File file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	    try {
			FileUtils.copyFile(file, new File("target/screenshots/"+scenario.getId()+".jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	    driver.close();
	    System.out.println("End"); 
	    
	}
	
	@After("@emptyComment")
	public void screenshotCloseBrowserEmptyComment(Scenario scenario){
		File file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	    try {
			FileUtils.copyFile(file, new File("target/screenshots/"+scenario.getId()+".jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	    driver.close();
	    System.out.println("End"); 
	    
	}
	
}
