package steps;

import pageObjects.ValidateMenu;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import junit.framework.Assert;

public class ValidateMenuSteps {
	
	private WebDriver driver;
	
	@Given("^the user accesses the Autonomiae Auream home page menu$")
	public void aUser() throws Throwable {
		JSONParser parser = new JSONParser();
		
		Object object = parser
            .parse(new FileReader("C:\\Users\\Alessandra\\eclipse-workspace\\TestMetixMedical\\asserts\\env.json"));
    
		JSONObject jsonObject = (JSONObject)object;
    
		String chromeDriver = (String) jsonObject.get("chromeDriver");
		String homePage = (String) jsonObject.get("homePage");
			System.setProperty("webdriver.chrome.driver", chromeDriver);
			driver = new ChromeDriver();
			driver.get(homePage);
			driver.manage().window().maximize();
			Thread.sleep(3000);
	
	}	

	@When("^the user clicks the Information Station Menu$")
	public void theUserClicksTheInformationStationMenu() throws Throwable {
		ValidateMenu.infoStationMenu(driver).click();
	
	}

	@Then("^the user is directed to the information station \"([^\"]*)\" page$")
	public void theUserIsDirectedToTheInformationStationPage(String arg1) throws Throwable {
		JSONParser parser = new JSONParser();
		
		Object object = parser
            .parse(new FileReader("C:\\Users\\Alessandra\\eclipse-workspace\\TestMetixMedical\\asserts\\env.json"));
    
		JSONObject jsonObject = (JSONObject)object;
    
		String infoPage = (String) jsonObject.get("infoPage");
		
		String currentUrl = driver.getCurrentUrl();
		Assert.assertEquals(infoPage, currentUrl);
	
	}

	@When("^the user clicks the Contact Menu$")
	public void theUserClicksTheContactMenu() throws Throwable {
		ValidateMenu.contactMenu(driver).click();
	
	}

	@Then("^the user is directed to the contact \"([^\"]*)\" page$")
	public void theUserIsDirectedToTheContactPage(String arg1) throws Throwable {
		JSONParser parser = new JSONParser();
		
		Object object = parser
            .parse(new FileReader("C:\\Users\\Alessandra\\eclipse-workspace\\TestMetixMedical\\asserts\\env.json"));
    
		JSONObject jsonObject = (JSONObject)object;
    
		String contactPage = (String) jsonObject.get("contactPage");
		
		String currentUrl = driver.getCurrentUrl();
		Assert.assertEquals(contactPage, currentUrl);
	
	}

	@When("^the user clicks the Home Menu$")
	public void theUserClicksTheHomeMenu() throws Throwable {
		Thread.sleep(3000);
		ValidateMenu.homeMenu(driver).click();
	
	}

	@Then("^the user is directed to the home \"([^\"]*)\" page$")
	public void theUserIsDirectedToTheHomePage(String arg1) throws Throwable {
		JSONParser parser = new JSONParser();
		
		Object object = parser
            .parse(new FileReader("C:\\Users\\Alessandra\\eclipse-workspace\\TestMetixMedical\\asserts\\env.json"));
    
		JSONObject jsonObject = (JSONObject)object;
    
		String loginPage = (String) jsonObject.get("loginPage");
		
		String currentUrl = driver.getCurrentUrl();
		Assert.assertEquals(loginPage, currentUrl);
	
	}

	
	@After("@infoStationMenu")
	public void screenshotCloseBrowserInfoStationMenu(Scenario scenario){
		File file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	    try {
			FileUtils.copyFile(file, new File("target/screenshots/"+scenario.getId()+".jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	    driver.close();
	    System.out.println("End"); 
	    
	}
	
	@After("@contactMenu")
	public void screenshotCloseBrowserContactMenu(Scenario scenario){
		File file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	    try {
			FileUtils.copyFile(file, new File("target/screenshots/"+scenario.getId()+".jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	    driver.close();
	    System.out.println("End"); 
	    
	}
	
	@After("@homeMenu")
	public void screenshotCloseBrowserHomeMenu(Scenario scenario){
		File file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	    try {
			FileUtils.copyFile(file, new File("target/screenshots/"+scenario.getId()+".jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	    driver.close();
	    System.out.println("End"); 
	    
	}
	
}
