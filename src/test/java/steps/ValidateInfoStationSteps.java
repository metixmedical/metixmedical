package steps;

import pageObjects.ValidateInfoStation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import junit.framework.Assert;

public class ValidateInfoStationSteps {
	
	private WebDriver driver;
	
	@Given("^the user accesses the Autonomiae Auream system$")
		public void aUser() throws Throwable {
			JSONParser parser = new JSONParser();
		
			Object object = parser
                .parse(new FileReader("C:\\Users\\Alessandra\\eclipse-workspace\\TestMetixMedical\\asserts\\env.json"));
        
			JSONObject jsonObject = (JSONObject)object;
        
			String chromeDriver = (String) jsonObject.get("chromeDriver");
			String homePage = (String) jsonObject.get("homePage");
				System.setProperty("webdriver.chrome.driver", chromeDriver);
				driver = new ChromeDriver();
				driver.get(homePage);
				driver.manage().window().maximize();
				Thread.sleep(3000);
			
	}
	
	
	@When("^the user clicks the Information Station menu option$")
	public void theUserClicksTheInformationStationMenuOption() throws Throwable {
		ValidateInfoStation.infoStationMenu(driver).click();
	}

	@Then("^the user is directed to the information station \"([^\"]*)\" page and view the page information successfully$")
	public void theUserIsDirectedToTheInformationStationPageAndViewThePageInformationSuccessfully(String arg1) throws Throwable {
	
		String validateIntroHeader = ValidateInfoStation.infoStationIntroHeader(driver).getText();
		Assert.assertEquals("Design Solution Systems!", validateIntroHeader);
		String validateMainHeader = ValidateInfoStation.infoStationMainHeader(driver).getText();
		Assert.assertEquals("Multitasking Overload?", validateMainHeader);
		String validateConclusionHeader = ValidateInfoStation.infoStationConclusionHeader(driver).getText();
		Assert.assertEquals("Automate Everything!", validateConclusionHeader);
		
		        JSONParser parser = new JSONParser();
		        try
		        {
		            Object object = parser
		                    .parse(new FileReader("C:\\Users\\Alessandra\\eclipse-workspace\\TestMetixMedical\\asserts\\validateInfoStation.json"));
		            
		            JSONObject jsonObject = (JSONObject)object;
		            
		            String panelOneTextOne = (String) jsonObject.get("panelOneTextOne");
		            String panelOneTextTwo = (String) jsonObject.get("panelOneTextTwo");
		            String panelOneTextThree = (String) jsonObject.get("panelOneTextThree");
		            String panelTwoTextOne = (String) jsonObject.get("panelTwoTextOne");
		            String panelTwoTextTwo = (String) jsonObject.get("panelTwoTextTwo");
		            String panelTwoTextThree = (String) jsonObject.get("panelTwoTextThree");
		            String panelThreeTextOne = (String) jsonObject.get("panelThreeTextOne");
		            String panelThreeTextTwo = (String) jsonObject.get("panelThreeTextTwo");
		            String panelThreeTextThree = (String) jsonObject.get("panelThreeTextThree");
		            
		            String validatePanelOneParagraphOne = ValidateInfoStation.infoStationPanelOneTextOne(driver).getText();
					Assert.assertEquals(panelOneTextOne, validatePanelOneParagraphOne);
					String validatePanelOneParagraphTwo = ValidateInfoStation.infoStationPanelOneTextTwo(driver).getText();
					Assert.assertEquals(panelOneTextTwo, validatePanelOneParagraphTwo);
					String validatePanelOneParagraphThree = ValidateInfoStation.infoStationPanelOneTextThree(driver).getText();
					Assert.assertEquals(panelOneTextThree, validatePanelOneParagraphThree);
					String validatePanelTwoParagraphOne = ValidateInfoStation.infoStationPanelTwoTextOne(driver).getText();
					Assert.assertEquals(panelTwoTextOne, validatePanelTwoParagraphOne);
					String validatePanelTwoParagraphTwo = ValidateInfoStation.infoStationPanelTwoTextTwo(driver).getText();
					Assert.assertEquals(panelTwoTextTwo, validatePanelTwoParagraphTwo);
					String validatePanelTwoParagraphThree = ValidateInfoStation.infoStationPanelTwoTextThree(driver).getText();
					Assert.assertEquals(panelTwoTextThree, validatePanelTwoParagraphThree);
					String validatePanelThreeParagraphOne = ValidateInfoStation.infoStationPanelThreeTextOne(driver).getText();
					Assert.assertEquals(panelThreeTextOne, validatePanelThreeParagraphOne);
					String validatePanelThreeParagraphTwo = ValidateInfoStation.infoStationPanelThreeTextTwo(driver).getText();
					Assert.assertEquals(panelThreeTextTwo, validatePanelThreeParagraphTwo);
					String validatePanelThreeParagraphThree = ValidateInfoStation.infoStationPanelThreeTextThree(driver).getText();
					Assert.assertEquals(panelThreeTextThree, validatePanelThreeParagraphThree);
		          
		        }
		        catch(FileNotFoundException fe)
		        {
		            fe.printStackTrace();
		        }
		        catch(Exception e)
		        {
		            e.printStackTrace();
		        }
		        System.out.println("End");
	}
	
	@After("@infoStationValidate")
	public void screenshotCloseBrowserinfoStation(Scenario scenario){
		File file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	    try {
			FileUtils.copyFile(file, new File("target/screenshots/"+scenario.getId()+".jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	    driver.close();
	    System.out.println("End"); 
	    
	}
	
}




