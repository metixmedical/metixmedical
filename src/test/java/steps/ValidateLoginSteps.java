package steps;

import pageObjects.ValidateLogin;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import junit.framework.Assert;

public class ValidateLoginSteps {
	
	private WebDriver driver;
	
	@Given("^the user accesses the Autonomiae Auream home page$")
	public void aUser() throws Throwable {
		JSONParser parser = new JSONParser();
		
		Object object = parser
            .parse(new FileReader("C:\\Users\\Alessandra\\eclipse-workspace\\TestMetixMedical\\asserts\\env.json"));
    
		JSONObject jsonObject = (JSONObject)object;
    
		String chromeDriver = (String) jsonObject.get("chromeDriver");
		String homePage = (String) jsonObject.get("homePage");
			System.setProperty("webdriver.chrome.driver", chromeDriver);
			driver = new ChromeDriver();
			driver.get(homePage);
			driver.manage().window().maximize();
			Thread.sleep(3000);
	
	}
	
	@When("^the user enters his \"([^\"]*)\" and \"([^\"]*)\"$")
	public void theUserEntersHisAnd(String arg1, String arg2) throws Throwable {
		ValidateLogin.emailAddressInputField(driver).click();
		ValidateLogin.emailAddressInputField(driver).sendKeys("alebsiufu@gmail.com");
		ValidateLogin.passwordInputField(driver).click();
		ValidateLogin.passwordInputField(driver).sendKeys("123456");
	   
	}

	@And("^the user clicks the button \"([^\"]*)\"$")
	public void whenTheUserClicksTheButton(String arg1) throws Throwable {
		Thread.sleep(2000);
		ValidateLogin.submitButton(driver).click();
		
	}

	@Then("^the user is directed to the \"([^\"]*)\" page$")
	public void theUserIsDirectedToThePage(String arg1) throws Throwable {
		JSONParser parser = new JSONParser();
		
		Object object = parser
            .parse(new FileReader("C:\\Users\\Alessandra\\eclipse-workspace\\TestMetixMedical\\asserts\\env.json"));
    
		JSONObject jsonObject = (JSONObject)object;
    
		String infoLoginPage = (String) jsonObject.get("infoLoginPage");
		
		String currentUrl = driver.getCurrentUrl();
		Assert.assertEquals(infoLoginPage, currentUrl);
		
	}
	
	@When("^the user enters a invalid \"([^\"]*)\"$")
	public void theUserEntersAInvalid(String arg1) throws Throwable {
		ValidateLogin.emailAddressInputField(driver).click();
		ValidateLogin.emailAddressInputField(driver).sendKeys(arg1);
		ValidateLogin.passwordInputField(driver).click();
		ValidateLogin.passwordInputField(driver).sendKeys("123456");
	    
	}

	@Then("^the user is not directed to the \"([^\"]*)\" page$")
	public void theUserIsNotDirectedToThePage(String arg1) throws Throwable {
		JSONParser parser = new JSONParser();
		
		Object object = parser
            .parse(new FileReader("C:\\Users\\Alessandra\\eclipse-workspace\\TestMetixMedical\\asserts\\env.json"));
    
		JSONObject jsonObject = (JSONObject)object;
    
		String loginPage = (String) jsonObject.get("loginPage");
		
		String currentUrl = driver.getCurrentUrl();
		Assert.assertEquals(loginPage, currentUrl);
	
	}
	
	@When("^the user enters a \"([^\"]*)\" and an empty \"([^\"]*)\"$")
	public void theUserEntersAAndAnEmpty(String arg1, String arg2) throws Throwable {
		ValidateLogin.emailAddressInputField(driver).click();
		ValidateLogin.emailAddressInputField(driver).sendKeys("alebsiufu@gmail.com");
		ValidateLogin.passwordInputField(driver).click();
		ValidateLogin.passwordInputField(driver).sendKeys("");
		
	}

	@When("^the user clicks on the \"([^\"]*)\" field$")
	public void theUserClicksOnTheField(String arg1) throws Throwable {
	    ValidateLogin.rememberMeField(driver).click();
	}

	@Then("^the user is directed to the \"([^\"]*)\" page and the email and password fields will be saved at the next login$")
	public void theUserIsDirectedToThePageAndTheEmailAndPasswordFieldsWillBeSavedAtTheNextLogin(String arg1) throws Throwable {
		JSONParser parser = new JSONParser();
		
		Object object = parser
            .parse(new FileReader("C:\\Users\\Alessandra\\eclipse-workspace\\TestMetixMedical\\asserts\\env.json"));
    
		JSONObject jsonObject = (JSONObject)object;
    
		String infoLoginPage = (String) jsonObject.get("infoLoginPage");
		
		String currentUrl = driver.getCurrentUrl();
		Assert.assertEquals(infoLoginPage, currentUrl);
		
	}

	@After("@loginValidate")
	public void screenshotCloseBrowserloginValidate(Scenario scenario){
		File file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	    try {
			FileUtils.copyFile(file, new File("target/screenshots/"+scenario.getId()+".jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	    driver.close();
	    System.out.println("End"); 
	    
	}
	
	@After("@invalidEmailLogin")
	public void screenshotCloseBrowserinvalidEmailLogin(Scenario scenario){
		File file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	    try {
			FileUtils.copyFile(file, new File("target/screenshots/"+scenario.getId()+".jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	    driver.close();
	    System.out.println("End"); 
	    
	}
	
	@After("@emptyPassword")
	public void screenshotCloseBrowserEmptyPassword(Scenario scenario){
		File file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	    try {
			FileUtils.copyFile(file, new File("target/screenshots/"+scenario.getId()+".jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	    driver.close();
	    System.out.println("End"); 
	    
	}
	
	@After("@rememberMeField")
	public void screenshotCloseBrowserRememberMeField(Scenario scenario){
		File file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	    try {
			FileUtils.copyFile(file, new File("target/screenshots/"+scenario.getId()+".jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	    driver.close();
	    System.out.println("End"); 
	    
	}

}




