package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ValidateMenu {
	
	private static WebElement element = null;
	
	public static WebElement infoStationMenu(WebDriver driver){
	    element = driver.findElement(By.id("info-link"));
	    return element;
	    
	}
	
	public static WebElement contactMenu(WebDriver driver){
	    element = driver.findElement(By.id("contact-link"));
	    return element;
	    
	}
	
	public static WebElement homeMenu(WebDriver driver){
	    element = driver.findElement(By.id("home-link"));
	    return element;
	    
	}
	
}


