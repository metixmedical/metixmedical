package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ValidateInfoStation {

	
	private static WebElement element = null;
	
	public static WebElement infoStationIntroHeader(WebDriver driver){
	    element = driver.findElement(By.id("panel-intro-heading"));
	    return element;
	    
	}
	
	public static WebElement infoStationPanelOneTextOne(WebDriver driver){
	    element = driver.findElement(By.id("panel-1-text-1"));
	    return element;
	    
	}
	
	public static WebElement infoStationPanelOneTextTwo(WebDriver driver){
	    element = driver.findElement(By.id("panel-1-text-2"));
	    return element;
	    
	}
	
	public static WebElement infoStationPanelOneTextThree(WebDriver driver){
	    element = driver.findElement(By.id("panel-1-text-3"));
	    return element;
	    
	}
	
	public static WebElement infoStationMainHeader(WebDriver driver){
	    element = driver.findElement(By.id("panel-main-heading"));
	    return element;
	    
	}
	
	public static WebElement infoStationPanelTwoTextOne(WebDriver driver){
	    element = driver.findElement(By.id("panel-2-text-1"));
	    return element;
	    
	}
	
	public static WebElement infoStationPanelTwoTextTwo(WebDriver driver){
	    element = driver.findElement(By.id("panel-2-text-2"));
	    return element;
	    
	}
	
	public static WebElement infoStationPanelTwoTextThree(WebDriver driver){
	    element = driver.findElement(By.id("panel-2-text-3"));
	    return element;
	    
	}
	
	public static WebElement infoStationConclusionHeader(WebDriver driver){
	    element = driver.findElement(By.id("panel-conclusion-heading"));
	    return element;
	    
	}
	
	public static WebElement infoStationPanelThreeTextOne(WebDriver driver){
	    element = driver.findElement(By.id("panel-3-text-1"));
	    return element;
	    
	}
	
	public static WebElement infoStationPanelThreeTextTwo(WebDriver driver){
	    element = driver.findElement(By.id("panel-3-text-2"));
	    return element;
	    
	}
	
	public static WebElement infoStationPanelThreeTextThree(WebDriver driver){
	    element = driver.findElement(By.id("panel-3-text-3"));
	    return element;
	    
	}
	
	public static WebElement infoStationMenu(WebDriver driver){
	    element = driver.findElement(By.id("info-link"));
	    return element;
	    
	}
	
}
