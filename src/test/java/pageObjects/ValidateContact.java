package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ValidateContact {
	
	private static WebElement element = null;
	
	public static WebElement emailAddressInput(WebDriver driver){
	    element = driver.findElement(By.id("email"));
	    return element;
	    
	}
	
	public static WebElement firstNameInputField(WebDriver driver){
	    element = driver.findElement(By.id("first-name"));
	    return element;
	    
	}
	
	public static WebElement lastNameInputField(WebDriver driver){
	    element = driver.findElement(By.id("last-name"));
	    return element;
	    
	}
	
	public static WebElement phoneNumberInputField(WebDriver driver){
	    element = driver.findElement(By.id("tel"));
	    return element;
	    
	}
	
	public static WebElement requestTypeTextBox(WebDriver driver){
	    element = driver.findElement(By.name("requestType"));
	    return element;
	    
	}
	
	public static WebElement requestTypeTextBoxOption(WebDriver driver){
	    element = driver.findElement(By.id("new"));
	    return element;
	    
	}
	
	public static WebElement commentField(WebDriver driver){
	    element = driver.findElement(By.name("message"));
	    return element;
	    
	}
	
	public static WebElement submitContactButton(WebDriver driver){
	    element = driver.findElement(By.id("contact-submit"));
	    return element;
	    
	}
	
}


