package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ValidateLogin {
	
	private static WebElement element = null;
	
	public static WebElement emailAddressInputField(WebDriver driver){
	    element = driver.findElement(By.id("email"));
	    return element;
	    
	}
	
	public static WebElement passwordInputField(WebDriver driver){
	    element = driver.findElement(By.id("pwd"));
	    return element;
	    
	}
	
	public static WebElement submitButton(WebDriver driver){
	    element = driver.findElement(By.id("home-submit"));
	    return element;
	    
	}
	
	public static WebElement rememberMeField(WebDriver driver){
	    element = driver.findElement(By.id("remember-me-check"));
	    return element;
	    
	}
	
}


