Set up an automated a test framework to Metix Medical.

This project aims to test the following scenarios of the 
Autonomiae Amittit Auream Automation Project.

Technologies used for the tests:

- Java (any version above 1.7);
- Selenium Webdriver;
- Maven;
- Cucumber;
- JUnit;
- ChromeDriver.

To run the tests you must have the following settings:
- Java JDK installed;
- Cucumber plugin installed;
- ChromeDriver installed;
- Maven installed.

Before run the tests, you must change the ChromeDriver path and 
the directory where you downloaded the Amittit Auream Automation 
Project to change the path of the html files to the path that you 
installed on your machine.The ChromeDriver path and the other html 
files in the json file that should be changed in the project are:

- asserts -> env.json -> "chromeDriver"
- asserts -> env.json -> "homePage"
- asserts -> env.json -> "loginPage"
- asserts -> env.json -> "contactPage"
- asserts -> env.json -> "infoPage"
- asserts -> env.json -> "infoLoginPage"
- asserts -> env.json -> "contactMessage"
- asserts -> env.json -> "contactEmptyMessage"

In the files where the folder steps are located, change the 
directory of the json file to the directory where you downloaded 
the project and your json file is present.
Files where the directory will be changed:

- src/test/java/steps/ValidateContactSteps.java
- src/test/java/steps/InfoStationSteps.java
- src/test/java/steps/ValidateLoginSteps.java
- src/test/java/steps/ValidateMenuSteps.java

Changes made to the Autonomiae Amittit Auream Automation Project 
and other information is detailed in the readme.txt file in the 
TestMetixMedical automation Project.

Install 

git clone "https://gitlab.com/metixmedical/metixmedical.git"
