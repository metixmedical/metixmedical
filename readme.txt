Autonomiae Amittit Auream Automation Project

For the automation of tests, good programming practices were used to guarantee the automation 
quality. The project structure is also based on these good practices. The standard 
3A (Arrange, Act, Assert) was used to separate the actions of automation from the scenarios 
that are going to be tested. Another good automation practice used was Page Objects, that aims 
to separate the elements of the application into a file, where each element receives the name of 
a variable, that is, this element is instantiated(concept of object orientation). This is to 
ensure clean and quality test automation.


Test Scenarios:

Behavior Driven Development (BDD) was used for test automation. Cucumber was used to 
describe test scenarios and perform validation of business rules. Test scenarios can be found in 
the ".feature" files in the features folder in src/test/resources.


Code Changes on the Autonomiae Amittit Auream Automation Project:

1
On the Contact page, the values of the "First Name" and "Last Name" fields ids have been changed to 
"first-name" and "last-name" respectively. Previously the ids had the same value (name) on the 
same page. It is not correct according to good programming practices different fields have same id 
on the same page. Besides being impracticable in a test automation that performs field validation.

2
Still on the Contact page, the value of the Submit button id was changed to "contact-submit", 
instead of "home-submit", because according to good programming practices it is advisable to name 
values ​​that best represent and identify the element, such as the page where he finds himself for 
example.

3
Still on the Contact page, id values of the Request Type options "Existing Ticket" and "New Ticket" 
were added as "existing" and "new" respectively for automation to identify these elements more 
clearly.

4
In the Info Station page the last gif name was changed in the info.html file, because it did not 
load on the screen because the name of the gif was different from the original name. In the html 
file the gif was set to "build-systems-error.gif" but the gif name in the resources folder was only 
"build-systems-error". So the name of the gif in html was changed to the original name 
contained in the resources folder.

5
Also in the Information Station page the header of the first panel presented a different layout 
from the other headers. Because the id value in the info.html file was like "panel-1-heading", 
but in stylesheets folder the field id is like "panel-intro-heading". The change was made according 
to the stylesheets folder.

6
Also in the Information Station page the third paragraph of the second panel is with the same id 
of the second paragraph of the same panel. The third paragraph had the modified id for 
"panel-2-text-3".

7
Also in the Information Station page the second paragraph of the third panel was the only element 
that did not have id. A new id was entered for this value element "panel-3-text-2".


Reports:

To view test automation reports, simply access the index.html file in the report-html folder 
contained in the target folder(target/report-html).


Screenshots:

To view test automation screenshots, simply access the screenshots folder contained in the target
folder(target/screenshots).





