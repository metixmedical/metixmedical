#Author: Alessandra Santos Costa (e-mail:alebsiufu@gmail.com)

@validateLogin
Feature: Validate Login
	As a user
	I want to log into the system
	So I can see the information system page

@loginValidate	
Scenario: Validate Login
	Given the user accesses the Autonomiae Auream home page
	When the user enters his "login" and "password"
	And the user clicks the button "Submit"
	Then the user is directed to the "information station" page

@invalidEmailLogin	
Scenario Outline: Invalid Email
	Given the user accesses the Autonomiae Auream home page
	When the user enters a invalid "<email>"
	And the user clicks the button "Submit"
	Then the user is not directed to the "information station" page

	Examples: 
			|    email    |
			|             |
      |    alebsi   |
      |   alebsi@   |
 
@emptyPassword     
Scenario: Empty Password
	Given the user accesses the Autonomiae Auream home page
	When the user enters a "login" and an empty "password"
	And the user clicks the button "Submit"
	Then the user is not directed to the "information station" page

@rememberMeField	
Scenario: Validate Remember me Field 
  Given the user accesses the Autonomiae Auream home page
	When the user enters his "login" and "password"
	And the user clicks on the "Remember me" field
	And the user clicks the button "Submit"
	Then the user is directed to the "information station" page and the email and password fields will be saved at the next login   

