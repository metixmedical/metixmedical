$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/validate_contact.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#Author: Alessandra Santos Costa (e-mail:alebsiufu@gmail.com)"
    }
  ],
  "line": 4,
  "name": "Validate Contact",
  "description": "As a user\nI want to access the contact page\nSo I can send my contact",
  "id": "validate-contact",
  "keyword": "Feature",
  "tags": [
    {
      "line": 3,
      "name": "@validateContact"
    }
  ]
});
formatter.scenario({
  "line": 10,
  "name": "Validate Contact",
  "description": "",
  "id": "validate-contact;validate-contact",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 9,
      "name": "@contactValidate"
    }
  ]
});
formatter.step({
  "line": 11,
  "name": "the user accesses the Contact page",
  "keyword": "Given "
});
formatter.step({
  "line": 12,
  "name": "the user enters the contact information",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "the user is directed to the \"information station\" page and the contact is successfully sent",
  "keyword": "Then "
});
formatter.match({
  "location": "ValidateContactSteps.aUser()"
});
formatter.result({
  "duration": 31980388071,
  "status": "passed"
});
formatter.match({
  "location": "ValidateContactSteps.theUserEntersTheContactInformation()"
});
formatter.result({
  "duration": 8310287682,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "information station",
      "offset": 29
    }
  ],
  "location": "ValidateContactSteps.theUserIsDirectedToThePageAndTheContactIsSuccessfullySent(String)"
});
formatter.result({
  "duration": 86772623,
  "status": "passed"
});
formatter.after({
  "duration": 1102726927,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 16,
  "name": "Invalid Email",
  "description": "",
  "id": "validate-contact;invalid-email",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 15,
      "name": "@invalidEmailContact"
    }
  ]
});
formatter.step({
  "line": 17,
  "name": "the user accesses the Contact page",
  "keyword": "Given "
});
formatter.step({
  "line": 18,
  "name": "the user enters an invalid \"\u003cemail\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "the user is not directed to the \"information station\" page and the contact is not successfully sent",
  "keyword": "Then "
});
formatter.examples({
  "line": 21,
  "name": "",
  "description": "",
  "id": "validate-contact;invalid-email;",
  "rows": [
    {
      "cells": [
        "email"
      ],
      "line": 22,
      "id": "validate-contact;invalid-email;;1"
    },
    {
      "cells": [
        ""
      ],
      "line": 23,
      "id": "validate-contact;invalid-email;;2"
    },
    {
      "cells": [
        "alebsi"
      ],
      "line": 24,
      "id": "validate-contact;invalid-email;;3"
    },
    {
      "cells": [
        "alebsi@"
      ],
      "line": 25,
      "id": "validate-contact;invalid-email;;4"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 23,
  "name": "Invalid Email",
  "description": "",
  "id": "validate-contact;invalid-email;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@validateContact"
    },
    {
      "line": 15,
      "name": "@invalidEmailContact"
    }
  ]
});
formatter.step({
  "line": 17,
  "name": "the user accesses the Contact page",
  "keyword": "Given "
});
formatter.step({
  "line": 18,
  "name": "the user enters an invalid \"\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "the user is not directed to the \"information station\" page and the contact is not successfully sent",
  "keyword": "Then "
});
formatter.match({
  "location": "ValidateContactSteps.aUser()"
});
formatter.result({
  "duration": 12016089477,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 28
    }
  ],
  "location": "ValidateContactSteps.theUserEntersAnInvalid(String)"
});
formatter.result({
  "duration": 1248543765,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "information station",
      "offset": 33
    }
  ],
  "location": "ValidateContactSteps.theUserIsNotDirectedToThePageAndTheContactIsNotSuccessfullySent(String)"
});
formatter.result({
  "duration": 3005298971,
  "status": "passed"
});
formatter.after({
  "duration": 716471476,
  "status": "passed"
});
formatter.scenario({
  "line": 24,
  "name": "Invalid Email",
  "description": "",
  "id": "validate-contact;invalid-email;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@validateContact"
    },
    {
      "line": 15,
      "name": "@invalidEmailContact"
    }
  ]
});
formatter.step({
  "line": 17,
  "name": "the user accesses the Contact page",
  "keyword": "Given "
});
formatter.step({
  "line": 18,
  "name": "the user enters an invalid \"alebsi\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "the user is not directed to the \"information station\" page and the contact is not successfully sent",
  "keyword": "Then "
});
formatter.match({
  "location": "ValidateContactSteps.aUser()"
});
formatter.result({
  "duration": 16468165209,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "alebsi",
      "offset": 28
    }
  ],
  "location": "ValidateContactSteps.theUserEntersAnInvalid(String)"
});
formatter.result({
  "duration": 1115338884,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "information station",
      "offset": 33
    }
  ],
  "location": "ValidateContactSteps.theUserIsNotDirectedToThePageAndTheContactIsNotSuccessfullySent(String)"
});
formatter.result({
  "duration": 3004890428,
  "status": "passed"
});
formatter.after({
  "duration": 530756797,
  "status": "passed"
});
formatter.scenario({
  "line": 25,
  "name": "Invalid Email",
  "description": "",
  "id": "validate-contact;invalid-email;;4",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@validateContact"
    },
    {
      "line": 15,
      "name": "@invalidEmailContact"
    }
  ]
});
formatter.step({
  "line": 17,
  "name": "the user accesses the Contact page",
  "keyword": "Given "
});
formatter.step({
  "line": 18,
  "name": "the user enters an invalid \"alebsi@\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "the user is not directed to the \"information station\" page and the contact is not successfully sent",
  "keyword": "Then "
});
formatter.match({
  "location": "ValidateContactSteps.aUser()"
});
formatter.result({
  "duration": 10524537285,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "alebsi@",
      "offset": 28
    }
  ],
  "location": "ValidateContactSteps.theUserEntersAnInvalid(String)"
});
formatter.result({
  "duration": 1285012039,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "information station",
      "offset": 33
    }
  ],
  "location": "ValidateContactSteps.theUserIsNotDirectedToThePageAndTheContactIsNotSuccessfullySent(String)"
});
formatter.result({
  "duration": 3004996529,
  "status": "passed"
});
formatter.after({
  "duration": 539834220,
  "status": "passed"
});
formatter.scenario({
  "line": 28,
  "name": "Empty First Name Field",
  "description": "",
  "id": "validate-contact;empty-first-name-field",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 27,
      "name": "@emptyFirstName"
    }
  ]
});
formatter.step({
  "line": 29,
  "name": "the user accesses the Contact page",
  "keyword": "Given "
});
formatter.step({
  "line": 30,
  "name": "the user enters an empty first name \"First Name\" Field",
  "keyword": "When "
});
formatter.step({
  "line": 31,
  "name": "the user is not directed to the \"information station\" page and the contact is not successfully sent",
  "keyword": "Then "
});
formatter.match({
  "location": "ValidateContactSteps.aUser()"
});
formatter.result({
  "duration": 10630098711,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "First Name",
      "offset": 37
    }
  ],
  "location": "ValidateContactSteps.theUserEntersAnEmptyFirstNameField(String)"
});
formatter.result({
  "duration": 1171376800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "information station",
      "offset": 33
    }
  ],
  "location": "ValidateContactSteps.theUserIsNotDirectedToThePageAndTheContactIsNotSuccessfullySent(String)"
});
formatter.result({
  "duration": 3226261583,
  "status": "passed"
});
formatter.after({
  "duration": 527658745,
  "status": "passed"
});
formatter.scenario({
  "line": 34,
  "name": "Empty Last Name Field",
  "description": "",
  "id": "validate-contact;empty-last-name-field",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 33,
      "name": "@emptyLastName"
    }
  ]
});
formatter.step({
  "line": 35,
  "name": "the user accesses the Contact page",
  "keyword": "Given "
});
formatter.step({
  "line": 36,
  "name": "the user enters an empty last name \"Last Name\" Field",
  "keyword": "When "
});
formatter.step({
  "line": 37,
  "name": "the user is not directed to the \"information station\" page and the contact is not successfully sent",
  "keyword": "Then "
});
formatter.match({
  "location": "ValidateContactSteps.aUser()"
});
formatter.result({
  "duration": 10399564800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Last Name",
      "offset": 36
    }
  ],
  "location": "ValidateContactSteps.theUserEntersAnEmptyLastNameField(String)"
});
formatter.result({
  "duration": 1164778573,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "information station",
      "offset": 33
    }
  ],
  "location": "ValidateContactSteps.theUserIsNotDirectedToThePageAndTheContactIsNotSuccessfullySent(String)"
});
formatter.result({
  "duration": 3005304256,
  "status": "passed"
});
formatter.after({
  "duration": 392403978,
  "status": "passed"
});
formatter.scenario({
  "line": 40,
  "name": "Empty Phone Number Field",
  "description": "",
  "id": "validate-contact;empty-phone-number-field",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 39,
      "name": "@emptyPhoneNumber"
    }
  ]
});
formatter.step({
  "line": 41,
  "name": "the user accesses the Contact page",
  "keyword": "Given "
});
formatter.step({
  "line": 42,
  "name": "the user enters an empty phone number \"Phone Number\" Field",
  "keyword": "When "
});
formatter.step({
  "line": 43,
  "name": "the user is not directed to the \"information station\" page and the contact is not successfully sent",
  "keyword": "Then "
});
formatter.match({
  "location": "ValidateContactSteps.aUser()"
});
formatter.result({
  "duration": 12237624502,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Phone Number",
      "offset": 39
    }
  ],
  "location": "ValidateContactSteps.theUserEntersAnEmptyPhoneNumberField(String)"
});
formatter.result({
  "duration": 1258600255,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "information station",
      "offset": 33
    }
  ],
  "location": "ValidateContactSteps.theUserIsNotDirectedToThePageAndTheContactIsNotSuccessfullySent(String)"
});
formatter.result({
  "duration": 3004780929,
  "status": "passed"
});
formatter.after({
  "duration": 452437832,
  "status": "passed"
});
formatter.scenario({
  "line": 46,
  "name": "Empty Comment",
  "description": "",
  "id": "validate-contact;empty-comment",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 45,
      "name": "@emptyComment"
    }
  ]
});
formatter.step({
  "line": 47,
  "name": "the user accesses the Contact page",
  "keyword": "Given "
});
formatter.step({
  "line": 48,
  "name": "the user enters an empty comment \"comment\" Field",
  "keyword": "When "
});
formatter.step({
  "line": 49,
  "name": "the user is directed to the \"information station\" page and the comment is empty in url",
  "keyword": "Then "
});
formatter.match({
  "location": "ValidateContactSteps.aUser()"
});
formatter.result({
  "duration": 9319382097,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "comment",
      "offset": 34
    }
  ],
  "location": "ValidateContactSteps.theUserEntersAnEmptyCommentField(String)"
});
formatter.result({
  "duration": 1254571466,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "information station",
      "offset": 29
    }
  ],
  "location": "ValidateContactSteps.theUserIsDirectedToThePageAndTheCommentIsEmptyInUrl(String)"
});
formatter.result({
  "duration": 3014397915,
  "status": "passed"
});
formatter.after({
  "duration": 373905157,
  "status": "passed"
});
formatter.uri("src/test/resources/features/validate_info_station.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#Author: Alessandra Santos Costa (e-mail:alebsiufu@gmail.com)"
    }
  ],
  "line": 4,
  "name": "Validate Information Station Page",
  "description": "As a user\r\nI want to access the Information Station Page\r\nSo I can view the information on the page",
  "id": "validate-information-station-page",
  "keyword": "Feature",
  "tags": [
    {
      "line": 3,
      "name": "@validateInfoStation"
    }
  ]
});
formatter.scenario({
  "line": 10,
  "name": "Access Information Station Page",
  "description": "",
  "id": "validate-information-station-page;access-information-station-page",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 9,
      "name": "@infoStationValidate"
    }
  ]
});
formatter.step({
  "line": 11,
  "name": "the user accesses the Autonomiae Auream system",
  "keyword": "Given "
});
formatter.step({
  "line": 12,
  "name": "the user clicks the Information Station menu option",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "the user is directed to the information station \"information station\" page and view the page information successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "ValidateInfoStationSteps.aUser()"
});
formatter.result({
  "duration": 10911337584,
  "status": "passed"
});
formatter.match({
  "location": "ValidateInfoStationSteps.theUserClicksTheInformationStationMenuOption()"
});
formatter.result({
  "duration": 221066069,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "information station",
      "offset": 49
    }
  ],
  "location": "ValidateInfoStationSteps.theUserIsDirectedToTheInformationStationPageAndViewThePageInformationSuccessfully(String)"
});
formatter.result({
  "duration": 550412526,
  "status": "passed"
});
formatter.after({
  "duration": 358316352,
  "status": "passed"
});
formatter.uri("src/test/resources/features/validate_login.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#Author: Alessandra Santos Costa (e-mail:alebsiufu@gmail.com)"
    }
  ],
  "line": 4,
  "name": "Validate Login",
  "description": "As a user\nI want to log into the system\nSo I can see the information system page",
  "id": "validate-login",
  "keyword": "Feature",
  "tags": [
    {
      "line": 3,
      "name": "@validateLogin"
    }
  ]
});
formatter.scenario({
  "line": 10,
  "name": "Validate Login",
  "description": "",
  "id": "validate-login;validate-login",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 9,
      "name": "@loginValidate"
    }
  ]
});
formatter.step({
  "line": 11,
  "name": "the user accesses the Autonomiae Auream home page",
  "keyword": "Given "
});
formatter.step({
  "line": 12,
  "name": "the user enters his \"login\" and \"password\"",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "the user clicks the button \"Submit\"",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "the user is directed to the \"information station\" page",
  "keyword": "Then "
});
formatter.match({
  "location": "ValidateLoginSteps.aUser()"
});
formatter.result({
  "duration": 10421521130,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "login",
      "offset": 21
    },
    {
      "val": "password",
      "offset": 33
    }
  ],
  "location": "ValidateLoginSteps.theUserEntersHisAnd(String,String)"
});
formatter.result({
  "duration": 1310910314,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Submit",
      "offset": 28
    }
  ],
  "location": "ValidateLoginSteps.whenTheUserClicksTheButton(String)"
});
formatter.result({
  "duration": 2162757760,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "information station",
      "offset": 29
    }
  ],
  "location": "ValidateLoginSteps.theUserIsDirectedToThePage(String)"
});
formatter.result({
  "duration": 10154660,
  "status": "passed"
});
formatter.after({
  "duration": 408833279,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 17,
  "name": "Invalid Email",
  "description": "",
  "id": "validate-login;invalid-email",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 16,
      "name": "@invalidEmailLogin"
    }
  ]
});
formatter.step({
  "line": 18,
  "name": "the user accesses the Autonomiae Auream home page",
  "keyword": "Given "
});
formatter.step({
  "line": 19,
  "name": "the user enters a invalid \"\u003cemail\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "the user clicks the button \"Submit\"",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "the user is not directed to the \"information station\" page",
  "keyword": "Then "
});
formatter.examples({
  "line": 23,
  "name": "",
  "description": "",
  "id": "validate-login;invalid-email;",
  "rows": [
    {
      "cells": [
        "email"
      ],
      "line": 24,
      "id": "validate-login;invalid-email;;1"
    },
    {
      "cells": [
        ""
      ],
      "line": 25,
      "id": "validate-login;invalid-email;;2"
    },
    {
      "cells": [
        "alebsi"
      ],
      "line": 26,
      "id": "validate-login;invalid-email;;3"
    },
    {
      "cells": [
        "alebsi@"
      ],
      "line": 27,
      "id": "validate-login;invalid-email;;4"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 25,
  "name": "Invalid Email",
  "description": "",
  "id": "validate-login;invalid-email;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 16,
      "name": "@invalidEmailLogin"
    },
    {
      "line": 3,
      "name": "@validateLogin"
    }
  ]
});
formatter.step({
  "line": 18,
  "name": "the user accesses the Autonomiae Auream home page",
  "keyword": "Given "
});
formatter.step({
  "line": 19,
  "name": "the user enters a invalid \"\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "the user clicks the button \"Submit\"",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "the user is not directed to the \"information station\" page",
  "keyword": "Then "
});
formatter.match({
  "location": "ValidateLoginSteps.aUser()"
});
formatter.result({
  "duration": 14425133824,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "",
      "offset": 27
    }
  ],
  "location": "ValidateLoginSteps.theUserEntersAInvalid(String)"
});
formatter.result({
  "duration": 529915547,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Submit",
      "offset": 28
    }
  ],
  "location": "ValidateLoginSteps.whenTheUserClicksTheButton(String)"
});
formatter.result({
  "duration": 2079984851,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "information station",
      "offset": 33
    }
  ],
  "location": "ValidateLoginSteps.theUserIsNotDirectedToThePage(String)"
});
formatter.result({
  "duration": 5645590,
  "status": "passed"
});
formatter.after({
  "duration": 1442919773,
  "status": "passed"
});
formatter.scenario({
  "line": 26,
  "name": "Invalid Email",
  "description": "",
  "id": "validate-login;invalid-email;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 16,
      "name": "@invalidEmailLogin"
    },
    {
      "line": 3,
      "name": "@validateLogin"
    }
  ]
});
formatter.step({
  "line": 18,
  "name": "the user accesses the Autonomiae Auream home page",
  "keyword": "Given "
});
formatter.step({
  "line": 19,
  "name": "the user enters a invalid \"alebsi\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "the user clicks the button \"Submit\"",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "the user is not directed to the \"information station\" page",
  "keyword": "Then "
});
formatter.match({
  "location": "ValidateLoginSteps.aUser()"
});
formatter.result({
  "duration": 10915779069,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "alebsi",
      "offset": 27
    }
  ],
  "location": "ValidateLoginSteps.theUserEntersAInvalid(String)"
});
formatter.result({
  "duration": 406812466,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Submit",
      "offset": 28
    }
  ],
  "location": "ValidateLoginSteps.whenTheUserClicksTheButton(String)"
});
formatter.result({
  "duration": 2069571170,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "information station",
      "offset": 33
    }
  ],
  "location": "ValidateLoginSteps.theUserIsNotDirectedToThePage(String)"
});
formatter.result({
  "duration": 6065837,
  "status": "passed"
});
formatter.after({
  "duration": 871143719,
  "status": "passed"
});
formatter.scenario({
  "line": 27,
  "name": "Invalid Email",
  "description": "",
  "id": "validate-login;invalid-email;;4",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 16,
      "name": "@invalidEmailLogin"
    },
    {
      "line": 3,
      "name": "@validateLogin"
    }
  ]
});
formatter.step({
  "line": 18,
  "name": "the user accesses the Autonomiae Auream home page",
  "keyword": "Given "
});
formatter.step({
  "line": 19,
  "name": "the user enters a invalid \"alebsi@\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "the user clicks the button \"Submit\"",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "the user is not directed to the \"information station\" page",
  "keyword": "Then "
});
formatter.match({
  "location": "ValidateLoginSteps.aUser()"
});
formatter.result({
  "duration": 9522533840,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "alebsi@",
      "offset": 27
    }
  ],
  "location": "ValidateLoginSteps.theUserEntersAInvalid(String)"
});
formatter.result({
  "duration": 506073578,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Submit",
      "offset": 28
    }
  ],
  "location": "ValidateLoginSteps.whenTheUserClicksTheButton(String)"
});
formatter.result({
  "duration": 2087133213,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "information station",
      "offset": 33
    }
  ],
  "location": "ValidateLoginSteps.theUserIsNotDirectedToThePage(String)"
});
formatter.result({
  "duration": 5831737,
  "status": "passed"
});
formatter.after({
  "duration": 837054582,
  "status": "passed"
});
formatter.scenario({
  "line": 30,
  "name": "Empty Password",
  "description": "",
  "id": "validate-login;empty-password",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 29,
      "name": "@emptyPassword"
    }
  ]
});
formatter.step({
  "line": 31,
  "name": "the user accesses the Autonomiae Auream home page",
  "keyword": "Given "
});
formatter.step({
  "line": 32,
  "name": "the user enters a \"login\" and an empty \"password\"",
  "keyword": "When "
});
formatter.step({
  "line": 33,
  "name": "the user clicks the button \"Submit\"",
  "keyword": "And "
});
formatter.step({
  "line": 34,
  "name": "the user is not directed to the \"information station\" page",
  "keyword": "Then "
});
formatter.match({
  "location": "ValidateLoginSteps.aUser()"
});
formatter.result({
  "duration": 9914452249,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "login",
      "offset": 19
    },
    {
      "val": "password",
      "offset": 40
    }
  ],
  "location": "ValidateLoginSteps.theUserEntersAAndAnEmpty(String,String)"
});
formatter.result({
  "duration": 459869380,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Submit",
      "offset": 28
    }
  ],
  "location": "ValidateLoginSteps.whenTheUserClicksTheButton(String)"
});
formatter.result({
  "duration": 2121576521,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "information station",
      "offset": 33
    }
  ],
  "location": "ValidateLoginSteps.theUserIsNotDirectedToThePage(String)"
});
formatter.result({
  "duration": 5650876,
  "status": "passed"
});
formatter.after({
  "duration": 1455494728,
  "status": "passed"
});
formatter.scenario({
  "line": 37,
  "name": "Validate Remember me Field",
  "description": "",
  "id": "validate-login;validate-remember-me-field",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 36,
      "name": "@rememberMeField"
    }
  ]
});
formatter.step({
  "line": 38,
  "name": "the user accesses the Autonomiae Auream home page",
  "keyword": "Given "
});
formatter.step({
  "line": 39,
  "name": "the user enters his \"login\" and \"password\"",
  "keyword": "When "
});
formatter.step({
  "line": 40,
  "name": "the user clicks on the \"Remember me\" field",
  "keyword": "And "
});
formatter.step({
  "line": 41,
  "name": "the user clicks the button \"Submit\"",
  "keyword": "And "
});
formatter.step({
  "line": 42,
  "name": "the user is directed to the \"information station\" page and the email and password fields will be saved at the next login",
  "keyword": "Then "
});
formatter.match({
  "location": "ValidateLoginSteps.aUser()"
});
formatter.result({
  "duration": 8997432450,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "login",
      "offset": 21
    },
    {
      "val": "password",
      "offset": 33
    }
  ],
  "location": "ValidateLoginSteps.theUserEntersHisAnd(String,String)"
});
formatter.result({
  "duration": 522687137,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Remember me",
      "offset": 24
    }
  ],
  "location": "ValidateLoginSteps.theUserClicksOnTheField(String)"
});
formatter.result({
  "duration": 130180457,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Submit",
      "offset": 28
    }
  ],
  "location": "ValidateLoginSteps.whenTheUserClicksTheButton(String)"
});
formatter.result({
  "duration": 2187598808,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "information station",
      "offset": 29
    }
  ],
  "location": "ValidateLoginSteps.theUserIsDirectedToThePageAndTheEmailAndPasswordFieldsWillBeSavedAtTheNextLogin(String)"
});
formatter.result({
  "duration": 21436778,
  "status": "passed"
});
formatter.after({
  "duration": 598247496,
  "status": "passed"
});
formatter.uri("src/test/resources/features/validate_menu.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#Author: Alessandra Santos Costa (e-mail:alebsiufu@gmail.com)"
    }
  ],
  "line": 4,
  "name": "Validate Menu",
  "description": "As a user\r\nI want to access the menu page\r\nSo I can choose which page I want to access",
  "id": "validate-menu",
  "keyword": "Feature",
  "tags": [
    {
      "line": 3,
      "name": "@validateMenu"
    }
  ]
});
formatter.scenario({
  "line": 10,
  "name": "Access Information Station Menu",
  "description": "",
  "id": "validate-menu;access-information-station-menu",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 9,
      "name": "@infoStationMenu"
    }
  ]
});
formatter.step({
  "line": 11,
  "name": "the user accesses the Autonomiae Auream home page menu",
  "keyword": "Given "
});
formatter.step({
  "line": 12,
  "name": "the user clicks the Information Station Menu",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "the user is directed to the information station \"information station\" page",
  "keyword": "Then "
});
formatter.match({
  "location": "ValidateMenuSteps.aUser()"
});
formatter.result({
  "duration": 10359519702,
  "status": "passed"
});
formatter.match({
  "location": "ValidateMenuSteps.theUserClicksTheInformationStationMenu()"
});
formatter.result({
  "duration": 293806276,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "information station",
      "offset": 49
    }
  ],
  "location": "ValidateMenuSteps.theUserIsDirectedToTheInformationStationPage(String)"
});
formatter.result({
  "duration": 8384184,
  "status": "passed"
});
formatter.after({
  "duration": 403374214,
  "status": "passed"
});
formatter.scenario({
  "line": 16,
  "name": "Access Information Contact Menu",
  "description": "",
  "id": "validate-menu;access-information-contact-menu",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 15,
      "name": "@contactMenu"
    }
  ]
});
formatter.step({
  "line": 17,
  "name": "the user accesses the Autonomiae Auream home page menu",
  "keyword": "Given "
});
formatter.step({
  "line": 18,
  "name": "the user clicks the Contact Menu",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "the user is directed to the contact \"contact\" page",
  "keyword": "Then "
});
formatter.match({
  "location": "ValidateMenuSteps.aUser()"
});
formatter.result({
  "duration": 8920601532,
  "status": "passed"
});
formatter.match({
  "location": "ValidateMenuSteps.theUserClicksTheContactMenu()"
});
formatter.result({
  "duration": 144953311,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "contact",
      "offset": 37
    }
  ],
  "location": "ValidateMenuSteps.theUserIsDirectedToTheContactPage(String)"
});
formatter.result({
  "duration": 8127429,
  "status": "passed"
});
formatter.after({
  "duration": 413446562,
  "status": "passed"
});
formatter.scenario({
  "line": 22,
  "name": "Access Home Menu",
  "description": "",
  "id": "validate-menu;access-home-menu",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 21,
      "name": "@homeMenu"
    }
  ]
});
formatter.step({
  "line": 23,
  "name": "the user accesses the Autonomiae Auream home page menu",
  "keyword": "Given "
});
formatter.step({
  "line": 24,
  "name": "the user clicks the Contact Menu",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "the user clicks the Home Menu",
  "keyword": "And "
});
formatter.step({
  "line": 26,
  "name": "the user is directed to the home \"home\" page",
  "keyword": "Then "
});
formatter.match({
  "location": "ValidateMenuSteps.aUser()"
});
formatter.result({
  "duration": 9886402518,
  "status": "passed"
});
formatter.match({
  "location": "ValidateMenuSteps.theUserClicksTheContactMenu()"
});
formatter.result({
  "duration": 180308854,
  "status": "passed"
});
formatter.match({
  "location": "ValidateMenuSteps.theUserClicksTheHomeMenu()"
});
formatter.result({
  "duration": 3113139853,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "home",
      "offset": 34
    }
  ],
  "location": "ValidateMenuSteps.theUserIsDirectedToTheHomePage(String)"
});
formatter.result({
  "duration": 5067891,
  "status": "passed"
});
formatter.after({
  "duration": 425055287,
  "status": "passed"
});
});